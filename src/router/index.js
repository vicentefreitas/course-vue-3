import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },{
        path:'/passando-props-de-pai-para-filho-com-options-api',
        name:'props',
        component: () => import('../views/PropsFatherToSonView.vue')
    }
  ]
})

export default router
